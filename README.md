## Blog
Blogging software with a Go backend being handled by the [Revel Framework](https://revel.github.io/)
The backend features a panel where you can see the number of posts, unique visitors, and a map showing the country each visitor is from.

![Backend Dashboard](../master/assets/backend.png?raw=true)

### New Post Entry Form
![New Post Form](../master/assets/post_entry.png?raw=true)

